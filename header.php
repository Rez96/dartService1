<header class="header">
    <nav class="header__navigate">
        <div class="header__logo">
            <img class="header__logo-img" src="/images/logo.png" alt="">
            <div class="logo-title">
                <p class="logo-title__text">dart<br><b class="logo-title__text-bold">service manager</b></p>
            </div> 
        </div>
        <ul class="nav-menu">
            <li class="nav-menu__list"><a class="nav-menu__link" href="#">Home</a></li>
            <li class="nav-menu__list"><a class="nav-menu__link" href="#">Service</a></li>
            <li class="nav-menu__list"><a class="nav-menu__link" href="#">Extension</a></li>
            <li class="nav-menu__list"><a class="nav-menu__link" href="#">Pricing</a></li>
            <li class="nav-menu__list"><a class="nav-menu__link" href="#">Help</a></li>
        </ul>
        <button class="header__register-button">sign up</button>
    </nav>
    <div class="first-page">
        <div class="first-page__text-container">
            <h2 class="first-page__title">lorem ipsum dolor sit amet</h2>
            <p class="first-page__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamc</p>
            <button class="first-page__button">buy now</button>
            <button class="first-page__button">try for free</button>
        </div>
        <video class="first-page__video" controls src="Martynko___Sovetskie_multfilmy_dlya_detej_(MosCatalogue.net).mp4"></video> 
    </div> 
</header>




